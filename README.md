# process-flow 0.0.1版本

#### 介绍
简单的流程编排框架

#### 使用方式
1. resources目录下新建PROCESS-FLOW文件夹
2. 新建process_flow_*.json的json文件来管理流程
3. 建立流程处理结点对应的实现类，加上@TreeNode注解
4. 建立业务的上下文Context 集成AbstractContext 用于流程结点间传递数据
5. 注入ProcessTrigger进行流程调用

#### 示例 process-flow-simple项目为示例代码