package com.kpi.processflow.record;

import com.google.common.base.Stopwatch;
import lombok.Data;

/**
 * @author Kevin Liu
 * @date 2020/5/3 6:58 下午
 */
@Data
public class Stage {
    private String identifier;
    private Stopwatch stopwatch;
    private boolean released;

    public Stage(String identifier) {
        this.identifier = identifier;
        this.stopwatch = Stopwatch.createStarted();
    }

    public void release() {
        this.stopwatch.stop();
        this.released = true;
    }

}
