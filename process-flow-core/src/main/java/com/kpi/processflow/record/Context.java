package com.kpi.processflow.record;

/**
 * @author Kevin Liu
 * @date 2020/5/4 9:41 上午
 */
public interface Context {

     String getBizCode();

     String getOperation();
}
