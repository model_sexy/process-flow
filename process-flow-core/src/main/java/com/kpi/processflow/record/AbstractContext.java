package com.kpi.processflow.record;

import lombok.experimental.SuperBuilder;

/**
 * @author Kevin Liu
 * @date 2020/5/4 10:37 上午
 */
@SuperBuilder
public class AbstractContext implements Context {

    protected  String bizCode;

    protected  String operation;

    @Override
    public String getBizCode() {
        return bizCode;
    }

    @Override
    public String getOperation() {
        return operation;
    }
}
