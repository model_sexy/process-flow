package com.kpi.processflow.annotation;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 流程结点注解
 * @author Kevin Liu
 * @date 2020/5/3 3:18 下午
 */
@Component
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TreeNode {

    /**
     * 流程代码值唯一标识某一流程 节点名不能重复
     * @return code
     */
    String code() default "";
}
