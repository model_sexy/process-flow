package com.kpi.processflow.process.registry;

import com.kpi.processflow.node.Node;
import com.kpi.processflow.process.Process;

import java.util.List;

/**
 * @author Kevin Liu
 * @date 2020/5/3 6:27 下午
 */
public interface ProcessDefinitionRegistry {

    /**
     * 注册一个流程
     * @param bizCode   业务代码
     * @param operation 流程名称
     * @param nodeList  该流程下面的所有结点
     */
    void registryProcess(String bizCode, String operation, List<Node> nodeList);

    void registryProcess(Process process, List<Node> nodes);
}
