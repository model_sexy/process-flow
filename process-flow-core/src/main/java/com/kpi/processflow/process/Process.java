package com.kpi.processflow.process;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.util.Strings;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.List;

/**
 * @author Kevin Liu
 * @date 2020/5/3 5:26 下午
 */
@Data
public class Process implements Serializable {

    /**
     * 业务域
     */
    private String bizCode;

    /**
     * 流程名称
     */
    private String operation;

    private List<String> nodeList;

    public void check(boolean nodeListCheck) {
        if (StringUtils.isEmpty(this.bizCode)) {
            throw new RuntimeException("process.biz.code.can.not.be.null.or.empty");
        } else if (StringUtils.isEmpty(this.operation)) {
            throw new RuntimeException("process.operation.can.not.be.null.or.empty");
        } else if (nodeListCheck && CollectionUtils.isEmpty(this.nodeList)) {
            throw new RuntimeException("process.node.list.can.not.be.null.or.empty");
        }
    }


}
