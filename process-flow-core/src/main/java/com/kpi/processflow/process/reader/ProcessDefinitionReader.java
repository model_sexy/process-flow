package com.kpi.processflow.process.reader;

import com.kpi.processflow.process.Process;

import java.util.Collection;

/**
 * @author Kevin Liu
 * @date 2020/5/3 9:23 下午
 */
public interface ProcessDefinitionReader {

    /**
     * 加载process列表
     * @return
     */
    Collection<Process> loadProcessDefinition();
}
