package com.kpi.processflow.process.factory;

import com.kpi.processflow.node.Node;
import com.kpi.processflow.process.Process;

import java.util.List;

/**
 * @author Kevin Liu
 * @date 2020/5/3 6:31 下午
 */
public interface ProcessFactory {

    /**
     * 获取一个流程
     * @param bizCode    业务码
     * @param operation  操作码
     * @return
     */
    List<Node> getProcessNodes(String bizCode,String operation);


    /**
     * 获取一个流程
     * @param process  process
     * @return  获取process
     */
    List<Node> getProcessNodes(Process process);
}
