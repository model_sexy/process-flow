package com.kpi.processflow.node;

import com.google.common.collect.Maps;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Kevin Liu
 * @date 2020/5/3 4:38 下午
 */
@Component
@Slf4j
@Data
public class NodeRegistry {

    /**
     * 向流程中注册一个Node
     *
     * @param nodeName 结点名称
     * @param nodeBean 对应处理结点任务的Bean
     */
    private final Map<String, Node> nodeNameToNodeMap = Maps.newHashMap();

    public void register(String nodeName, Node nodeBean) {
        Node existedNode = this.nodeNameToNodeMap.get(nodeName);
        if (existedNode != null) {
            log.error("node1: {} , node2: {} have the same node name, please check", nodeBean, existedNode);
            throw new RuntimeException("Found that two nodes have the same name");
        } else {
            this.nodeNameToNodeMap.put(nodeName, nodeBean);
        }
    }

    public Node getNode(String nodeName) {
        return this.nodeNameToNodeMap.get(nodeName);
    }

    public void release() {
        this.nodeNameToNodeMap.clear();
    }
}
