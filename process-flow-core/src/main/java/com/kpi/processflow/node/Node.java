package com.kpi.processflow.node;

import com.kpi.processflow.annotation.TreeNode;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.StringUtils;

/**
 * @author Kevin Liu
 * @date 2020/5/3 4:28 下午
 */
public interface Node<Context> {

    /**
     * 执行结点的方法
     * @param context 流程上下文
     */
    void execute(Context context);

    /**
     * 获取当前流程结点的code值
     * @return code
     */
    default  String getNodeCode(){
        TreeNode treeNode = AnnotationUtils.findAnnotation(this.getClass(), TreeNode.class);
        Class clazz = this.getClass();
        if(null == treeNode) {
            return clazz.getSimpleName();
        }else {
            return StringUtils.isEmpty(treeNode.code())?clazz.getSimpleName():treeNode.code();
        }
    }

}
