package com.kpi.processflow;

import com.kpi.processflow.node.NodeRegistry;
import com.kpi.processflow.process.reader.FileSystemProcessDefinitionReader;
import com.kpi.processflow.process.reader.ProcessDefinitionReader;
import com.kpi.processflow.process.registry.ProcessDefinitionRegistry;
import com.kpi.processflow.runner.ProcessNodeAssembler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Kevin Liu
 * @date 2020/5/3 3:16 下午
 */
@Configuration
@ComponentScan("com.kpi.processflow")
public class ProcessFlowAutoConfiguration {

    @Bean
    public ProcessNodeAssembler processNodeAssembler(NodeRegistry nodeRegistry, ProcessDefinitionRegistry processDefinitionRegistry){
        return new ProcessNodeAssembler(nodeRegistry,processDefinitionRegistry,processDefinitionReader());
    }

    @Bean
    public ProcessDefinitionReader processDefinitionReader(){
         return new FileSystemProcessDefinitionReader();
    }
}
