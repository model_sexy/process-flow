package com.kpi.processflowsimple.component;

import com.kpi.processflow.annotation.TreeNode;
import com.kpi.processflow.node.Node;
import com.kpi.processflowsimple.context.SimpleContext;
import com.kpi.processflowsimple.entity.User;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Kevin Liu
 * @date 2020/5/4 9:16 上午
 */
@Slf4j
@TreeNode(code = "user.get.id")
public class GetUser implements Node<SimpleContext> {

    @Override
    public void execute(SimpleContext context) {
        long id = context.getId();
        //模拟从数据库获取user
        User user = User.builder().id(id).name("kevin").age(23).build();
        log.info("查询结果： user:{}",user);
        context.setUser(user);
    }

}
