package com.kpi.processflowsimple.component;

import com.kpi.processflow.annotation.TreeNode;
import com.kpi.processflow.node.Node;
import com.kpi.processflowsimple.context.SimpleContext;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Kevin Liu
 * @date 2020/5/4 9:16 上午
 */
@Slf4j
@TreeNode(code = "user.param.check")
public class ParamCheck implements Node<SimpleContext> {

    @Override
    public void execute(SimpleContext context) {
        long userId = context.getId();
        log.info("参数校验开始： userId:{}",userId);
        if(userId <= 0) {
            throw new IllegalArgumentException("参数校验失败");
        }
    }

}
