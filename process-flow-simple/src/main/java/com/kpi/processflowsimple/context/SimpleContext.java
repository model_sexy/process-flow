package com.kpi.processflowsimple.context;

import com.kpi.processflow.record.AbstractContext;
import com.kpi.processflow.record.Context;
import com.kpi.processflowsimple.entity.User;
import lombok.*;
import lombok.experimental.SuperBuilder;

/**
 * 将需要传递的值放入context中
 * simple项目测试传递一个ID
 *
 * @author Kevin Liu
 * @date 2020/5/4 9:20 上午
 */
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Getter
@Setter
public class SimpleContext extends AbstractContext {

    private long id;

    private User user;
}
