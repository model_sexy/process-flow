package com.kpi.processflowsimple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProcessFlowSimpleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProcessFlowSimpleApplication.class, args);
    }

}
