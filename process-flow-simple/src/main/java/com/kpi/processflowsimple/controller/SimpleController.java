package com.kpi.processflowsimple.controller;

import com.kpi.processflowsimple.facade.SimpleFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Kevin Liu
 * @date 2020/5/4 9:46 上午
 */
@RestController
public class SimpleController {

    @Autowired
    private SimpleFacade simpleFacade;

    @GetMapping("/test")
    public Object test(long id){
        return  simpleFacade.getUser(id);
    }
}
