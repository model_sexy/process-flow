package com.kpi.processflowsimple.entity;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Kevin Liu
 * @date 2020/5/4 10:42 上午
 */
@Data
@Builder
public class User implements Serializable {

    private long id;

    private String name;

    private int age;
}
