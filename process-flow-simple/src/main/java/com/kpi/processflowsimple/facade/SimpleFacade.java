package com.kpi.processflowsimple.facade;

import com.kpi.processflowsimple.entity.User;

/**
 * SimpleFacade
 *
 * @author Kevin Liu
 * @date 2020/5/4 9:22 上午
 */
public interface SimpleFacade {

    /**
     * 测试Facade接口
     * @return test
     * @param userId
     */
    User getUser(long userId);
}
