package com.kpi.processflowsimple.facade.impl;

import com.kpi.processflow.process.ProcessTrigger;
import com.kpi.processflowsimple.context.SimpleContext;
import com.kpi.processflowsimple.entity.User;
import com.kpi.processflowsimple.facade.SimpleFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Kevin Liu
 * @date 2020/5/4 9:27 上午
 */
@Service
public class SimpleFacadeImpl implements SimpleFacade {

    private final  static  String BIZ_CODE = "process_flow_simple";
    private final  static  String OPERATION = "process_flow_simple";


    @Autowired
    private ProcessTrigger processTrigger;

    @Override
    public User getUser(long userId) {
        //第一步新建上下文
        SimpleContext simpleContext  =  buildContext(userId);
        processTrigger.fire(simpleContext);
        return simpleContext.getUser();
    }

    private SimpleContext buildContext(long orderId) {
        return SimpleContext
                .builder()
                .id(orderId)
                .bizCode(BIZ_CODE)
                .operation(OPERATION)
                .build();
    }
}
